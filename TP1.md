PARTIE 1


🌞 Repérez dans le fichier /etc/sudoers la ligne qui donne tous les droits au groupe wheel

````
$ sudo cat /etc/sudoers
[sudo] password for jeremy:
## Sudoers allows particular users to run various commands as
## the root user, without needing the root password.
[...]
## Allows people in group wheel to run all commands
%wheel  ALL=(ALL)       ALL
````


🌞 Créez un nouvel utilisateur

````
$ sudo useradd -m -G "wheel" ADMIN
[sudo] password for jeremy:
[jeremy@node1 ~]$ sudo passwd ADMIN
Changing password for user ADMIN.
New password:
Retype new password:
passwd: all authentication tokens updated successfully.
[jeremy@node1 ~]$ su - ADMIN
Password:
Last login: Mon Dec 13 14:10:31 CET 2021 on pts/0
[ADMIN@node1 ~]$
````


🌞 Désactivez SELinux


````
$ sestatus
SELinux status:                 enabled
SELinuxfs mount:                /sys/fs/selinux
SELinux root directory:         /etc/selinux
Loaded policy name:             targeted
Current mode:                   enforcing
Mode from config file:          enforcing
Policy MLS status:              enabled
Policy deny_unknown status:     allowed
Memory protection checking:     actual (secure)
Max kernel policy version:      33

$ sudo setenforce 0
$ sudo nano /etc/selinux/config
# This file controls the state of SELinux on the system.
# SELINUX= can take one of these three values:
#     enforcing - SELinux security policy is enforced.
#     permissive - SELinux prints warnings instead of enforcing.
#     disabled - No SELinux policy is loaded.
SELINUX=permissive
# SELINUXTYPE= can take one of these three values:
#     targeted - Targeted processes are protected,
#     minimum - Modification of targeted policy. Only selected processes are protected.
#     mls - Multi Level Security protection.
SELINUXTYPE=targeted
$ sestatus
SELinux status:                 enabled
SELinuxfs mount:                /sys/fs/selinux
SELinux root directory:         /etc/selinux
Loaded policy name:             targeted
Current mode:                   permissive
Mode from config file:          permissive
Policy MLS status:              enabled
Policy deny_unknown status:     allowed
Memory protection checking:     actual (secure)
Max kernel policy version:      33
````



PARTIE 2

1. Analyse du service SSH

🌞 Afficher le statut du service SSH

````
$ sudo systemctl status sshd
● sshd.service - OpenSSH server daemon
   Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: enabled)
   Active: active (running) since Mon 2021-12-13 13:36:18 CET; 54min ago
````

🌞 Déterminer sur quel port le service SSH écoute

````
$ sudo ss -lutpn
[sudo] password for ADMIN:
Netid    State     Recv-Q    Send-Q       Local Address:Port       Peer Address:Port   Process
tcp      LISTEN    0         128                0.0.0.0:22              0.0.0.0:*       users:(("sshd",pid=856,fd=5))
tcp      LISTEN    0         128                   [::]:22                 [::]:*       users:(("sshd",pid=856,fd=7))
````

🌞 Déterminer l'utilisateur qui a lancé le processus SSH

````
$ ps -ef
UID          PID    PPID  C STIME TTY          TIME CMD
root           1       0  0 13:36 ?        00:00:00 /usr/lib/systemd/systemd --switched-root --system --deserialize 17
[...]
root        1637     856  0 14:10 ?        00:00:00 sshd: jeremy [priv]
jeremy      1641    1637  0 14:10 ?        00:00:00 sshd: jeremy@pts/0
[...]
ADMIN       1797    1671  0 14:43 pts/0    00:00:00 ps -ef
````


2. Modification du service SSH

🌞 Changez le port d'écoute du service SSH

````
$ sudo nano /etc/ssh/sshd_config
#       $OpenBSD: sshd_config,v 1.103 2018/04/09 20:41:22 tj Exp $

# This is the sshd server system-wide configuration file.  See
# sshd_config(5) for more information.

# This sshd was compiled with PATH=/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin

# The strategy used for options in the default sshd_config shipped with
# OpenSSH is to specify options with their default value where
# possible, but leave them commented.  Uncommented options override the
# default value.

# If you want to change the port on a SELinux system, you have to tell
# SELinux about this change.
# semanage port -a -t ssh_port_t -p tcp #PORTNUMBER
#
Port 1024
#AddressFamily any
#ListenAddress 0.0.0.0
#ListenAddress ::

HostKey /etc/ssh/ssh_host_rsa_key
HostKey /etc/ssh/ssh_host_ecdsa_key
HostKey /etc/ssh/ssh_host_ed25519_key

$ sudo firewall-cmd --permanent --add-port=1024/tcp
success
$ sudo firewall-cmd --reload
success
$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 1024/tcp
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
````

🌞 Vérification

````
$ ss
Netid State  Recv-Q Send-Q                                         Local Address:Port      Peer Address:Port    Process
u_str ESTAB  0      0                                                          * 20870                * 20869
[...]
tcp   ESTAB  0      36                                              192.168.56.2:1024      192.168.56.1:altalink

PS C:\Users\jaudr> ssh jeremy@192.168.56.2 -p 1024
jeremy@192.168.56.2's password:
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Mon Dec 13 14:10:57 2021 from 192.168.56.1
````


3. Service Web


🌞 Installez le paquet NGINX

````
$ sudo dnf install nginx
[sudo] password for jeremy:
Rocky Linux 8 - AppStream                                                               2.2 MB/s | 8.4 MB     00:03
Rocky Linux 8 - BaseOS                                                                  3.9 MB/s | 3.5 MB     00:00
Rocky Linux 8 - Extras                                                                   17 kB/s |  10 kB     00:00
Dependencies resolved.
========================================================================================================================
 Package                            Architecture  Version                                        Repository        Size
========================================================================================================================
Installing:
 nginx                              x86_64        1:1.14.1-9.module+el8.4.0+542+81547229         appstream        566 k
Installing dependencies:
 fontconfig                         x86_64        2.13.1-4.el8                                   baseos           273 k
 gd                                 x86_64        2.2.5-7.el8                                    appstream        143 k
 jbigkit-libs                       x86_64        2.1-14.el8                                     appstream         54 k
 libX11                             x86_64        1.6.8-5.el8                                    appstream        610 k
[...]
 perl-parent-1:0.237-1.el8.noarch
  perl-podlators-4.11-1.el8.noarch
  perl-threads-1:2.21-2.el8.x86_64
  perl-threads-shared-1.58-2.el8.x86_64

Complete!
````

🌞 Lancez le service NGINX

````
$ sudo systemctl start nginx
````

🌞 Gestion de firewall

````
$ sudo ss -lutpn
Netid  State   Recv-Q  Send-Q    Local Address:Port     Peer Address:Port  Process
tcp    LISTEN  0       128             0.0.0.0:80            0.0.0.0:*      users:(("nginx",pid=26104,fd=8),("nginx",pid=26103,fd=8))
tcp    LISTEN  0       128             0.0.0.0:1024          0.0.0.0:*      users:(("sshd",pid=1869,fd=5))
tcp    LISTEN  0       128                [::]:80               [::]:*      users:(("nginx",pid=26104,fd=9),("nginx",pid=26103,fd=9))
tcp    LISTEN  0       128                [::]:1024             [::]:*      users:(("sshd",pid=1869,fd=7))

$ sudo firewall-cmd --permanent --add-port=80/tcp
success
$ sudo firewall-cmd --reload
success
````

🌞 Vérifier que le service est accessible

````
PS C:\WINDOWS\system32> curl 192.168.56.2                                                                               

StatusCode        : 200
StatusDescription : OK
Content           : <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

                    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
                      <head>
                        <title>Test Page for the Nginx...
[...]
ParsedHtml        : mshtml.HTMLDocumentClass
RawContentLength  : 3429
````



4. Votre propre service


🌞 Essayez de lancer le serveur Python à la main

````
PS C:\Users\jaudr> curl 192.168.56.2:8888                                                                               

StatusCode        : 200
StatusDescription : OK
Content           : <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
                    <html>
                    <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                    <title>Directory listing fo...
RawContent        : HTTP/1.0 200 OK
                    Content-Length: 487
                    Content-Type: text/html; charset=utf-8
                    Date: Mon, 13 Dec 2021 15:25:55 GMT
                    Server: SimpleHTTP/0.6 Python/3.6.8

                    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//...
Forms             : {}
Headers           : {[Content-Length, 487], [Content-Type, text/html; charset=utf-8], [Date, Mon, 13 Dec 2021 15:25:55
                    GMT], [Server, SimpleHTTP/0.6 Python/3.6.8]}
Images            : {}
InputFields       : {}
Links             : {@{innerHTML=.bash_history; innerText=.bash_history; outerHTML=<A
                    href=".bash_history">.bash_history</A>; outerText=.bash_history; tagName=A; href=.bash_history},
                    @{innerHTML=.bash_logout; innerText=.bash_logout; outerHTML=<A
                    href=".bash_logout">.bash_logout</A>; outerText=.bash_logout; tagName=A; href=.bash_logout},
                    @{innerHTML=.bash_profile; innerText=.bash_profile; outerHTML=<A
                    href=".bash_profile">.bash_profile</A>; outerText=.bash_profile; tagName=A; href=.bash_profile},
                    @{innerHTML=.bashrc; innerText=.bashrc; outerHTML=<A href=".bashrc">.bashrc</A>;
                    outerText=.bashrc; tagName=A; href=.bashrc}}
ParsedHtml        : mshtml.HTMLDocumentClass
RawContentLength  : 487
````


🌞 Créez un service web.service

````
$ sudo /usr/bin/python3 -m http.server 8888
[sudo] password for jeremy:
Serving HTTP on 0.0.0.0 port 8888 (http://0.0.0.0:8888/) ...
192.168.56.1 - - [13/Dec/2021 17:19:18] "GET / HTTP/1.1" 200 -
````

🌞 Préparez l'environnement

````
$ sudo useradd Web
$ sudo mkdir /srv/web/
$ sudo nano /srv/web/test.txt
$ sudo chown Web /srv/web -R
$ ls -al
total 4
drwxr-xr-x. 2 Web  root 22 Dec 13 17:42 .
drwxr-xr-x. 3 root root 17 Dec 13 17:36 ..
-rw-r--r--. 1 Web  root 24 Dec 13 17:42 test.txt
````

🌞 Modifiez l'unité de service web.service créée précédemment en ajoutant les clauses

````
$ sudo nano /etc/systemd/system/test.service

[Unit]
Description=Service de test
User=Web
WorkingDirectory=/srv/web/

[Service]
ExecStart=/usr/bin/python3 -m http.server 8888

[Install]
WantedBy=multi-user.target
````


🌞 Vérifiez le bon fonctionnement avec une commande curl

````
PS C:\WINDOWS\system32> curl 192.168.56.2:8888


StatusCode        : 200
StatusDescription : OK
Content           : <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
                    <html>
                    <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                    <title>Directory listing fo...
RawContent        : HTTP/1.0 200 OK
                    Content-Length: 942
                    Content-Type: text/html; charset=utf-8
                    Date: Mon, 13 Dec 2021 17:47:41 GMT
                    Server: SimpleHTTP/0.6 Python/3.6.8

                    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//...
Forms             : {}
Headers           : {[Content-Length, 942], [Content-Type, text/html; charset=utf-8], [Date, Mon, 13 Dec 2021 17:47:41
                    GMT], [Server, SimpleHTTP/0.6 Python/3.6.8]}
Images            : {}
InputFields       : {}
Links             : {@{innerHTML=bin@; innerText=bin@; outerHTML=<A href="bin/">bin@</A>; outerText=bin@; tagName=A;
                    href=bin/}, @{innerHTML=boot/; innerText=boot/; outerHTML=<A href="boot/">boot/</A>;
                    outerText=boot/; tagName=A; href=boot/}, @{innerHTML=dev/; innerText=dev/; outerHTML=<A
                    href="dev/">dev/</A>; outerText=dev/; tagName=A; href=dev/}, @{innerHTML=etc/; innerText=etc/;
                    outerHTML=<A href="etc/">etc/</A>; outerText=etc/; tagName=A; href=etc/}...}
ParsedHtml        : mshtml.HTMLDocumentClass
RawContentLength  : 942
````