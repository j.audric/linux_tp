# Introduction Infrastructure as Code



# I. Ansible



## 1. Un premier playbook
````
$ cd /home
$ sudo mkdir ansible

$ ssh-keygen
$ cat /home/jeremy/.ssh/id_rsa.pub
$ ssh-copy-id jeremy@10.2.1.16
````
````
$ cd ansible
$ sudo nano nginx.yml
---
- name: Install nginx
  hosts: cesi
  become: true

  tasks:
  - name: Add epel-release repo
    yum:
      name: epel-release
      state: present

  - name: Install nginx
    yum:
      name: nginx
      state: present

  - name: Insert Index Page
    template:
      src: index.html.j2
      dest: /usr/share/nginx/html/index.html

  - name: Start NGiNX
    service:
      name: nginx
      state: started
````
````
$ sudo nano hosts.ini
[cesi]
10.2.1.16
$ sudo nano index.html.j2
$ ansible-playbook -i hosts.ini nginx.yml -K

---



## 2. Création de playbooks



````
$ sudo nano mariadb.yml
---
- name: Install mariadb
  hosts: cesi
  become: true

  tasks:
  - name: install mariadb-server
    yum:
      name: mariadb-server
      state: present

 - name: Start mariadb
   service:
     name: mariadb
     state: started

 - name: Create a new database with name 'test'
     mysql_db:
      name: test
      state: present
````

