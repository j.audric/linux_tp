# Partie 1 : Mise en place de la solution


# I. Setup base de données

## 1. Install MariaDB

🌞 **Installer MariaDB sur la machine `db.tp2.cesi`**
````
$ sudo dnf install mariadb-server
Last metadata expiration check: 0:56:57 ago on Tue 14 Dec 2021 01:59:44 PM CET.
Dependencies resolved.
========================================================================================================================
 Package                           Architecture  Version                                         Repository        Size
========================================================================================================================
Installing:
 mariadb-server                    x86_64        3:10.3.28-1.module+el8.4.0+427+adf35707         appstream         16 M
Installing dependencies:
 mariadb                           x86_64        3:10.3.28-1.module+el8.4.0+427+adf35707         appstream        6.0 M
[...]
  perl-libs-4:5.26.3-420.el8.x86_64
  perl-macros-4:5.26.3-420.el8.x86_64
  perl-parent-1:0.237-1.el8.noarch
  perl-podlators-4.11-1.el8.noarch
  perl-threads-1:2.21-2.el8.x86_64
  perl-threads-shared-1.58-2.el8.x86_64
  psmisc-23.1-5.el8.x86_64

Complete!
````


🌞 **Le service MariaDB**

````
$ sudo systemctl start mariadb

$ sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.

$ sudo systemctl status mariadb
● mariadb.service - MariaDB 10.3 database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-12-14 15:03:30 CET; 2min 29s ago
     Docs: man:mysqld(8)
           https://mariadb.com/kb/en/library/systemd/
 Main PID: 4637 (mysqld)
   Status: "Taking your SQL requests now..."
    Tasks: 30 (limit: 4956)
   Memory: 83.3M
   CGroup: /system.slice/mariadb.service
           └─4637 /usr/libexec/mysqld --basedir=/usr

Dec 14 15:03:29 db.tp2.cesi mysql-prepare-db-dir[4534]: See the MariaDB Knowledgebase at http://mariadb.com/kb or the
Dec 14 15:03:29 db.tp2.cesi mysql-prepare-db-dir[4534]: MySQL manual for more instructions.
Dec 14 15:03:29 db.tp2.cesi mysql-prepare-db-dir[4534]: Please report any problems at http://mariadb.org/jira
Dec 14 15:03:29 db.tp2.cesi mysql-prepare-db-dir[4534]: The latest information about MariaDB is available at http://mar>
Dec 14 15:03:29 db.tp2.cesi mysql-prepare-db-dir[4534]: You can find additional information about the MySQL part at:
Dec 14 15:03:29 db.tp2.cesi mysql-prepare-db-dir[4534]: http://dev.mysql.com
Dec 14 15:03:29 db.tp2.cesi mysql-prepare-db-dir[4534]: Consider joining MariaDB's strong and vibrant community:
Dec 14 15:03:29 db.tp2.cesi mysql-prepare-db-dir[4534]: https://mariadb.org/get-involved/
Dec 14 15:03:29 db.tp2.cesi mysqld[4637]: 2021-12-14 15:03:29 0 [Note] /usr/libexec/mysqld (mysqld 10.3.28-MariaDB) sta>
Dec 14 15:03:30 db.tp2.cesi systemd[1]: Started MariaDB 10.3 database server.
lines 1-22/22 (END)

$ sudo ss -lutpn
Netid   State    Recv-Q   Send-Q      Local Address:Port       Peer Address:Port   Process
tcp     LISTEN   0        128               0.0.0.0:22              0.0.0.0:*       users:(("sshd",pid=852,fd=5))
tcp     LISTEN   0        80                      *:3306                  *:*       users:(("mysqld",pid=4637,fd=21))
tcp     LISTEN   0        128                  [::]:22                 [::]:*       users:(("sshd",pid=852,fd=7))

$ ps -e
    PID TTY          TIME CMD
      1 ?        00:00:01 systemd
      2 ?        00:00:00 kthreadd
      3 ?        00:00:00 rcu_gp
      4 ?        00:00:00 rcu_par_gp
      [...]
      1589 ?        00:00:00 kworker/u2:0-events_unbound
   4637 ?        00:00:00 mysqld
   4704 ?        00:00:00 kworker/u2:2-events_unbound
   4876 ?        00:00:00 kworker/0:2-ata_sff
   4970 ?        00:00:00 kworker/0:0-ata_sff
   5020 ?        00:00:00 kworker/0:1-events
   5027 pts/0    00:00:00 ps
   
   $ ps -ef | grep 4637
mysql       4637       1  0 15:03 ?        00:00:01 /usr/libexec/mysqld --basedir=/usr
````

🌞 **Firewall**

````
$ sudo firewall-cmd --add-port=3306/tcp --permanent
success
````

## 2. Conf MariaDB


🌞 **Configuration élémentaire de la base**

````
$ mysql_secure_installation

NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user.  If you've just installed MariaDB, and
you haven't set the root password yet, the password will be blank,
so you should just press enter here.

Enter current password for root (enter for none):
OK, successfully used password, moving on...

Setting the root password ensures that nobody can log into the MariaDB
root user without the proper authorisation.

Set root password? [Y/n] y
New password:
Re-enter new password:
Password updated successfully!
Reloading privilege tables..
 ... Success!
 
 By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? [Y/n] y
 ... Success!
 
 Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? [Y/n] y
 ... Success!

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? [Y/n] y
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? [Y/n] y
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
````

🌞 **Préparation de la base en vue de l'utilisation par NextCloud**

````
$ sudo mysql -u root -p
[sudo] password for jeremy:
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 17
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> CREATE USER 'nextcloud'@'10.2.1.11' IDENTIFIED BY 'meow';
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.001 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.2.1.11';
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)
````

## 3. Test

🌞 **Installez sur la machine `web.tp2.cesi` la commande `mysql`**

````
$ sudo dnf provides mysql
[sudo] password for jeremy:
Last metadata expiration check: 3:01:47 ago on Tue 14 Dec 2021 01:26:28 PM CET.
mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64 : MySQL client programs and shared libraries
Repo        : appstream
Matched from:
Provide    : mysql = 8.0.26-1.module+el8.4.0+652+6de068a7
````

````
$ sudo dnf install mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64
Last metadata expiration check: 3:06:03 ago on Tue 14 Dec 2021 01:26:28 PM CET.
Dependencies resolved.
========================================================================================================================
 Package                            Architecture   Version                                      Repository         Size
========================================================================================================================
Installing:
 mysql                              x86_64         8.0.26-1.module+el8.4.0+652+6de068a7         appstream          12 M
Installing dependencies:
 mariadb-connector-c-config         noarch         3.1.11-2.el8_3                               appstream          14 k
 mysql-common                       x86_64         8.0.26-1.module+el8.4.0+652+6de068a7         appstream         133 k
Enabling module streams:
 mysql                                             8.0

Transaction Summary
========================================================================================================================
Install  3 Packages

Total download size: 12 M
Installed size: 63 M
Is this ok [y/N]: y
Downloading Packages:
(1/3): mariadb-connector-c-config-3.1.11-2.el8_3.noarch.rpm                             100 kB/s |  14 kB     00:00
(2/3): mysql-common-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64.rpm                     499 kB/s | 133 kB     00:00
(3/3): mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64.rpm                            9.3 MB/s |  12 MB     00:01
------------------------------------------------------------------------------------------------------------------------
Total                                                                                   8.2 MB/s |  12 MB     00:01
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                1/1
  Installing       : mariadb-connector-c-config-3.1.11-2.el8_3.noarch                                               1/3
  Installing       : mysql-common-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64                                       2/3
  Installing       : mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64                                              3/3
  Running scriptlet: mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64                                              3/3
  Verifying        : mariadb-connector-c-config-3.1.11-2.el8_3.noarch                                               1/3
  Verifying        : mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64                                              2/3
  Verifying        : mysql-common-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64                                       3/3

Installed:
  mariadb-connector-c-config-3.1.11-2.el8_3.noarch               mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64
  mysql-common-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64

Complete!
````

🌞 **Tester la connexion**

````
$ mysql -h 10.2.1.12 -P 3306 -u nextcloud -p -D nextcloud
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 9
Server version: 5.5.5-10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> show tables;
Empty set (0.00 sec)
````


# II. Setup Apache


## 1. Install Apache

### A. Apache

🌞 **Installer Apache sur la machine `web.tp2.cesi`**

````
$ sudo dnf install httpd
[sudo] password for jeremy:
Last metadata expiration check: 0:06:17 ago on Wed 15 Dec 2021 09:03:27 AM CET.
Dependencies resolved.
========================================================================================================================
 Package                     Architecture     Version                                         Repository           Size
========================================================================================================================
Installing:
 httpd                       x86_64           2.4.37-43.module+el8.5.0+714+5ec56ee8           appstream           1.4 M
Installing dependencies:
 apr                         x86_64           1.6.3-12.el8                                    appstream           128 k
 apr-util                    x86_64           1.6.1-6.el8.1                                   appstream           104 k
 httpd-filesystem            noarch           2.4.37-43.module+el8.5.0+714+5ec56ee8           appstream            38 k
 httpd-tools                 x86_64           2.4.37-43.module+el8.5.0+714+5ec56ee8           appstream           106 k
 mod_http2                   x86_64           1.15.7-3.module+el8.5.0+695+1fa8055e            appstream           153 k
 rocky-logos-httpd           noarch           85.0-3.el8                                      baseos               22 k
Installing weak dependencies:
 apr-util-bdb                x86_64           1.6.1-6.el8.1                                   appstream            23 k
 apr-util-openssl            x86_64           1.6.1-6.el8.1                                   appstream            26 k
Enabling module streams:
 httpd                                        2.4

Transaction Summary
========================================================================================================================
Install  9 Packages

Total download size: 2.0 M
Installed size: 5.4 M
Is this ok [y/N]: y
Downloading Packages:
(1/9): apr-util-bdb-1.6.1-6.el8.1.x86_64.rpm                                            133 kB/s |  23 kB     00:00
(2/9): apr-util-1.6.1-6.el8.1.x86_64.rpm                                                526 kB/s | 104 kB     00:00
(3/9): apr-1.6.3-12.el8.x86_64.rpm                                                      630 kB/s | 128 kB     00:00
(4/9): apr-util-openssl-1.6.1-6.el8.1.x86_64.rpm                                        688 kB/s |  26 kB     00:00
(5/9): httpd-filesystem-2.4.37-43.module+el8.5.0+714+5ec56ee8.noarch.rpm                1.2 MB/s |  38 kB     00:00
(6/9): httpd-tools-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64.rpm                     2.8 MB/s | 106 kB     00:00
(7/9): httpd-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64.rpm                            16 MB/s | 1.4 MB     00:00
(8/9): mod_http2-1.15.7-3.module+el8.5.0+695+1fa8055e.x86_64.rpm                        1.3 MB/s | 153 kB     00:00
(9/9): rocky-logos-httpd-85.0-3.el8.noarch.rpm                                          166 kB/s |  22 kB     00:00
------------------------------------------------------------------------------------------------------------------------
Total                                                                                   2.6 MB/s | 2.0 MB     00:00
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                1/1
  Installing       : apr-1.6.3-12.el8.x86_64                                                                        1/9
  Running scriptlet: apr-1.6.3-12.el8.x86_64                                                                        1/9
  Installing       : apr-util-bdb-1.6.1-6.el8.1.x86_64                                                              2/9
  Installing       : apr-util-openssl-1.6.1-6.el8.1.x86_64                                                          3/9
  Installing       : apr-util-1.6.1-6.el8.1.x86_64                                                                  4/9
  Running scriptlet: apr-util-1.6.1-6.el8.1.x86_64                                                                  4/9
  Installing       : httpd-tools-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64                                       5/9
  Installing       : rocky-logos-httpd-85.0-3.el8.noarch                                                            6/9
  Running scriptlet: httpd-filesystem-2.4.37-43.module+el8.5.0+714+5ec56ee8.noarch                                  7/9
  Installing       : httpd-filesystem-2.4.37-43.module+el8.5.0+714+5ec56ee8.noarch                                  7/9
  Installing       : mod_http2-1.15.7-3.module+el8.5.0+695+1fa8055e.x86_64                                          8/9
  Installing       : httpd-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64                                             9/9
  Running scriptlet: httpd-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64                                             9/9
  Verifying        : apr-1.6.3-12.el8.x86_64                                                                        1/9
  Verifying        : apr-util-1.6.1-6.el8.1.x86_64                                                                  2/9
  Verifying        : apr-util-bdb-1.6.1-6.el8.1.x86_64                                                              3/9
  Verifying        : apr-util-openssl-1.6.1-6.el8.1.x86_64                                                          4/9
  Verifying        : httpd-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64                                             5/9
  Verifying        : httpd-filesystem-2.4.37-43.module+el8.5.0+714+5ec56ee8.noarch                                  6/9
  Verifying        : httpd-tools-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64                                       7/9
  Verifying        : mod_http2-1.15.7-3.module+el8.5.0+695+1fa8055e.x86_64                                          8/9
  Verifying        : rocky-logos-httpd-85.0-3.el8.noarch                                                            9/9

Installed:
  apr-1.6.3-12.el8.x86_64
  apr-util-1.6.1-6.el8.1.x86_64
  apr-util-bdb-1.6.1-6.el8.1.x86_64
  apr-util-openssl-1.6.1-6.el8.1.x86_64
  httpd-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64
  httpd-filesystem-2.4.37-43.module+el8.5.0+714+5ec56ee8.noarch
  httpd-tools-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64
  mod_http2-1.15.7-3.module+el8.5.0+695+1fa8055e.x86_64
  rocky-logos-httpd-85.0-3.el8.noarch

Complete!
````

---

🌞 **Analyse du service Apache**

````
$ sudo systemctl start httpd
$ sudo systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
$ systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-12-15 09:16:34 CET; 3min 11s ago
     Docs: man:httpd.service(8)
 Main PID: 2112 (httpd)
   Status: "Running, listening on: port 80"
    Tasks: 213 (limit: 4956)
   Memory: 33.7M
   CGroup: /system.slice/httpd.service
           ├─2112 /usr/sbin/httpd -DFOREGROUND
           ├─2113 /usr/sbin/httpd -DFOREGROUND
           ├─2114 /usr/sbin/httpd -DFOREGROUND
           ├─2115 /usr/sbin/httpd -DFOREGROUND
           └─2116 /usr/sbin/httpd -DFOREGROUND

Dec 15 09:16:34 web.tp2.cesi systemd[1]: Starting The Apache HTTP Server...
Dec 15 09:16:34 web.tp2.cesi systemd[1]: Started The Apache HTTP Server.
Dec 15 09:16:35 web.tp2.cesi httpd[2112]: Server configured, listening on: port 80
$ ps -ef | grep 2112
root        2112       1  0 09:16 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2113    2112  0 09:16 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2114    2112  0 09:16 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2115    2112  0 09:16 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2116    2112  0 09:16 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
jeremy      2366    1478  0 09:23 pts/0    00:00:00 grep --color=auto 2112
$ sudo ss -lutpn
Netid    State     Recv-Q    Send-Q       Local Address:Port       Peer Address:Port   Process
tcp      LISTEN    0         128                0.0.0.0:22              0.0.0.0:*       users:(("sshd",pid=853,fd=5))
tcp      LISTEN    0         128                   [::]:22                 [::]:*       users:(("sshd",pid=853,fd=7))
tcp      LISTEN    0         128                      *:80                    *:*       users:(("httpd",pid=2116,fd=4),("httpd",pid=2115,fd=4),("httpd",pid=2114,fd=4),("httpd",pid=2112,fd=4))
````
---

🌞 **Un premier test**

````
$ sudo firewall-cmd --add-port=80/tcp --permanent
success
$ sudo firewall-cmd --reload
success
````

````
$ curl 10.2.1.11:80
curl : HTTP Server Test Page
This page is used to test the proper operation of an HTTP server after it has been installed on a Rocky Linux system.
If you can read this page, it means that the software it working correctly.
Just visiting?
[...]
Apache™ is a registered trademark of the Apache Software Foundation in the United States and/or other countries.
NGINX™ is a registered trademark of F5 Networks, Inc..
Au caractère Ligne:1 : 1
+ curl 10.2.1.11:80
+ ~~~~~~~~~~~~~~~~~
    + CategoryInfo          : InvalidOperation : (System.Net.HttpWebRequest:HttpWebRequest) [Invoke-WebRequest], WebEx
   ception
    + FullyQualifiedErrorId : WebCmdletWebResponseException,Microsoft.PowerShell.Commands.InvokeWebRequestCommand
````

### B. PHP


🌞 **Installer PHP**

````
$ sudo dnf install epel-release
[sudo] password for jeremy:
Last metadata expiration check: 0:35:46 ago on Wed 15 Dec 2021 09:03:27 AM CET.
Dependencies resolved.
========================================================================================================================
 Package                         Architecture              Version                      Repository                 Size
========================================================================================================================
Installing:
 epel-release                    noarch                    8-13.el8                     extras                     23 k

Transaction Summary
========================================================================================================================
Install  1 Package

Total download size: 23 k
Installed size: 35 k
Is this ok [y/N]: y
Downloading Packages:
epel-release-8-13.el8.noarch.rpm                                                        219 kB/s |  23 kB     00:00
------------------------------------------------------------------------------------------------------------------------
Total                                                                                    77 kB/s |  23 kB     00:00
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                1/1
  Installing       : epel-release-8-13.el8.noarch                                                                   1/1
  Running scriptlet: epel-release-8-13.el8.noarch                                                                   1/1
  Verifying        : epel-release-8-13.el8.noarch                                                                   1/1

Installed:
  epel-release-8-13.el8.noarch

Complete!
````

````
$ sudo dnf update
Extra Packages for Enterprise Linux 8 - x86_64                                          7.3 MB/s |  11 MB     00:01
Extra Packages for Enterprise Linux Modular 8 - x86_64                                  776 kB/s | 980 kB     00:01
Last metadata expiration check: 0:00:01 ago on Wed 15 Dec 2021 09:40:15 AM CET.
Dependencies resolved.
Nothing to do.
Complete!
````

````
$ sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
Last metadata expiration check: 0:00:33 ago on Wed 15 Dec 2021 09:40:15 AM CET.
remi-release-8.rpm                                                                      224 kB/s |  26 kB     00:00
Dependencies resolved.
========================================================================================================================
 Package                      Architecture           Version                         Repository                    Size
========================================================================================================================
Installing:
 remi-release                 noarch                 8.5-2.el8.remi                  @commandline                  26 k

Transaction Summary
========================================================================================================================
Install  1 Package

Total size: 26 k
Installed size: 21 k
Is this ok [y/N]: y
Downloading Packages:
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                1/1
  Installing       : remi-release-8.5-2.el8.remi.noarch                                                             1/1
  Verifying        : remi-release-8.5-2.el8.remi.noarch                                                             1/1

Installed:
  remi-release-8.5-2.el8.remi.noarch

Complete!

$ sudo dnf module enable php:remi-7.4
Remi's Modular repository for Enterprise Linux 8 - x86_64                               3.8 kB/s | 858  B     00:00
Remi's Modular repository for Enterprise Linux 8 - x86_64                               3.0 MB/s | 3.1 kB     00:00
Importing GPG key 0x5F11735A:
 Userid     : "Remi's RPM repository <remi@remirepo.net>"
 Fingerprint: 6B38 FEA7 231F 87F5 2B9C A9D8 5550 9759 5F11 735A
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-remi.el8
Is this ok [y/N]: y
Remi's Modular repository for Enterprise Linux 8 - x86_64                               2.2 MB/s | 947 kB     00:00
Safe Remi's RPM repository for Enterprise Linux 8 - x86_64                              2.3 kB/s | 858  B     00:00
Safe Remi's RPM repository for Enterprise Linux 8 - x86_64                              3.0 MB/s | 3.1 kB     00:00
Importing GPG key 0x5F11735A:
 Userid     : "Remi's RPM repository <remi@remirepo.net>"
 Fingerprint: 6B38 FEA7 231F 87F5 2B9C A9D8 5550 9759 5F11 735A
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-remi.el8
Is this ok [y/N]: y
Safe Remi's RPM repository for Enterprise Linux 8 - x86_64                              1.3 MB/s | 2.0 MB     00:01
Dependencies resolved.
========================================================================================================================
 Package                     Architecture               Version                       Repository                   Size
========================================================================================================================
Enabling module streams:
 php                                                    remi-7.4

Transaction Summary
========================================================================================================================

Is this ok [y/N]: y
Complete!

$ sudo dnf install zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
Last metadata expiration check: 0:01:00 ago on Wed 15 Dec 2021 09:42:03 AM CET.
Package zip-3.0-23.el8.x86_64 is already installed.
Package unzip-6.0-45.el8_4.x86_64 is already installed.
Package libxml2-2.9.7-11.el8.x86_64 is already installed.
Package openssl-1:1.1.1k-4.el8.x86_64 is already installed.
Dependencies resolved.
[...]
Installed:
  checkpolicy-2.9-1.el8.x86_64                                   environment-modules-4.5.2-1.el8.x86_64
  fontconfig-2.13.1-4.el8.x86_64                                 fribidi-1.0.4-8.el8.x86_64
  gd3php-2.3.3-4.el8.remi.x86_64                                 graphite2-1.3.10-10.el8.x86_64
  harfbuzz-1.7.5-3.el8.x86_64                                    jbigkit-libs-2.1-14.el8.x86_64
  libX11-1.6.8-5.el8.x86_64                                      libX11-common-1.6.8-5.el8.noarch
  libXau-1.0.9-3.el8.x86_64                                      libXpm-3.5.12-8.el8.x86_64
  libicu69-69.1-1.el8.remi.x86_64                                libimagequant-2.12.5-1.el8.x86_64
  libjpeg-turbo-1.5.3-12.el8.x86_64                              libraqm-0.7.0-4.el8.x86_64
  libsodium-1.0.18-2.el8.x86_64                                  libtiff-4.0.9-20.el8.x86_64
  libwebp-1.0.0-5.el8.x86_64                                     libxcb-1.13.1-1.el8.x86_64
  libxslt-1.1.32-6.el8.x86_64                                    oniguruma5php-6.9.7.1-1.el8.remi.x86_64
  php74-libzip-1.8.0-1.el8.remi.x86_64                           php74-php-7.4.27-1.el8.remi.x86_64
  php74-php-bcmath-7.4.27-1.el8.remi.x86_64                      php74-php-cli-7.4.27-1.el8.remi.x86_64
  php74-php-common-7.4.27-1.el8.remi.x86_64                      php74-php-fpm-7.4.27-1.el8.remi.x86_64
  php74-php-gd-7.4.27-1.el8.remi.x86_64                          php74-php-gmp-7.4.27-1.el8.remi.x86_64
  php74-php-intl-7.4.27-1.el8.remi.x86_64                        php74-php-json-7.4.27-1.el8.remi.x86_64
  php74-php-mbstring-7.4.27-1.el8.remi.x86_64                    php74-php-mysqlnd-7.4.27-1.el8.remi.x86_64
  php74-php-opcache-7.4.27-1.el8.remi.x86_64                     php74-php-pdo-7.4.27-1.el8.remi.x86_64
  php74-php-pecl-zip-1.20.0-1.el8.remi.x86_64                    php74-php-process-7.4.27-1.el8.remi.x86_64
  php74-php-sodium-7.4.27-1.el8.remi.x86_64                      php74-php-xml-7.4.27-1.el8.remi.x86_64
  php74-runtime-1.0-3.el8.remi.x86_64                            policycoreutils-python-utils-2.9-16.el8.noarch
  python3-audit-3.0-0.17.20191104git1c2f876.el8.1.x86_64         python3-libsemanage-2.9-6.el8.x86_64
  python3-policycoreutils-2.9-16.el8.noarch                      python3-setools-4.3.0-2.el8.x86_64
  scl-utils-1:2.0.2-14.el8.x86_64                                tcl-1:8.6.8-2.el8.x86_64

Complete!
````

## 2. Conf Apache


🌞 **Analyser la conf Apache**

````
$ cat /etc/httpd/conf/httpd.conf | grep conf.d/
IncludeOptional conf.d/*.conf
````

🌞 **Créer un VirtualHost qui accueillera NextCloud**

````
$ cd /etc/httpd/conf.d
sudo nano Nextcloud.conf
[sudo] password for jeremy:
[jeremy@web conf.d]$ ls
autoindex.conf  Nextcloud.conf  php74-php.conf  README  userdir.conf  welcome.conf
````

🌞 **Configurer la racine web**

````
$ cd /var/www
$ sudo mkdir nextcloud
$ sudo mkdir nextcloud/html
$ sudo chown -R apache:apache html
````

🌞 **Configurer PHP**

````
$ timedatectl
               Local time: Wed 2021-12-15 10:52:19 CET
           Universal time: Wed 2021-12-15 09:52:19 UTC
                 RTC time: Wed 2021-12-15 09:43:06
                Time zone: Europe/Paris (CET, +0100)
System clock synchronized: no
              NTP service: inactive
          RTC in local TZ: no
````

````
$ sudo nano /etc/opt/remi/php74/php.ini
````

````
;;;;;;;;;;;;;;;;;;;
; Module Settings ;
;;;;;;;;;;;;;;;;;;;

[CLI Server]
; Whether the CLI web server uses ANSI color coding in its terminal output.
cli_server.color = On

[Date]
; Defines the default timezone used by the date functions
; http://php.net/date.timezone
date.timezone = "Europe/Paris (CET, +0100)"
````

# III. NextCloud


🌞 **Récupérer Nextcloud**

````
$ cd
$ curl -SLO https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  148M  100  148M    0     0  8093k      0  0:00:18  0:00:18 --:--:-- 20.3M

$ ls
nextcloud-21.0.1.zip
````

🌞 **Ranger la chambre**

````
$ unzip nextcloud-21.0.1.zip
$ rm nextcloud-21.0.1.zip
````

## 4. Test


🌞 **Modifiez le fichier `hosts` de votre PC**

````
$ get-content c:\windows\system32\drivers\etc\hosts
#
127.0.0.1 localhost
::1 localhost

10.2.1.11 web.tp2.cesi
````

🌞 **Tester l'accès à NextCloud et finaliser son install'**


# Partie 2 : Sécurisation

## 1. Conf SSH


🌞 **Modifier la conf du serveur SSH**

````
$ sudo nano /etc/ssh/sshd_config
````

````
#       $OpenBSD: sshd_config,v 1.103 2018/04/09 20:41:22 tj Exp $

# This is the sshd server system-wide configuration file.  See
# sshd_config(5) for more information.

# This sshd was compiled with PATH=/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin
[...]
## Hardening
UseDNS no
VersionAddendum pancho
PasswordAuthentication no
PermitRootLogin prohibit-password
LogLevel VERBOSE
AuthenticationMethods publickey

# more logging for sftp sessiosn
Subsystem sftp /usr/libexec/sftp-server -f AUTHPRIV -l INFO

# algorithms
KexAlgorithms curve25519-sha256@libssh.org,diffie-hellman-group-exchange-sha256
MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-512,hmac-sha2-256,u$Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr
hostkeyalgorithms ecdsa-sha2-nistp256-cert-v01@openssh.com,ecdsa-sha2-nistp384-cert-v01@openssh.com,ecdsa-sha2-nistp521$
````
````
$ [jeremy@web .ssh]$ sudo sshd -T
[sudo] password for jeremy:
/etc/ssh/sshd_config line 156: Subsystem 'sftp' already defined.
$ service sshd reload
Redirecting to /bin/systemctl reload sshd.service
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ====
Authentication is required to reload 'sshd.service'.
Authenticating as: Jérémy (jeremy)
Password:
==== AUTHENTICATION COMPLETE ====
````

## 2. Bonus : Fail2Ban



🌞 **Installez et configurez fail2ban**

````
$ sudo systemctl start firewalld
$ sudo systemctl enable firewalld
$ sudo dnf install epel-release -y
Last metadata expiration check: 5:02:12 ago on Wed 15 Dec 2021 09:42:03 AM CET.
Package epel-release-8-13.el8.noarch is already installed.
Dependencies resolved.
Nothing to do.
Complete!
````
````
$ sudo dnf install fail2ban fail2ban-firewalld -y
Last metadata expiration check: 5:05:15 ago on Wed 15 Dec 2021 09:42:03 AM CET.
Dependencies resolved.
========================================================================================================================
 Package                      Architecture     Version                                        Repository           Size
========================================================================================================================
Installing:
 fail2ban                     noarch           0.11.2-1.el8                                   epel                 19 k
 [...]
 Installed:
  esmtp-1.2-15.el8.x86_64                                           fail2ban-0.11.2-1.el8.noarch
  fail2ban-firewalld-0.11.2-1.el8.noarch                            fail2ban-sendmail-0.11.2-1.el8.noarch
  fail2ban-server-0.11.2-1.el8.noarch                               libesmtp-1.0.6-18.el8.x86_64
  liblockfile-1.14-1.el8.x86_64                                     python3-pip-9.0.3-20.el8.rocky.0.noarch
  python3-setuptools-39.2.0-6.el8.noarch                            python3-systemd-234-8.el8.x86_64
  python36-3.6.8-38.module+el8.5.0+671+195e4563.x86_64

Complete!
$ sudo systemctl start fail2ban
$ sudo systemctl enable fail2ban
Created symlink /etc/systemd/system/multi-user.target.wants/fail2ban.service → /usr/lib/systemd/system/fail2ban.service.
$ sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local
````

````
$ sudo nano /etc/fail2ban/jail.local

# [DEFAULT]
 bantime = 1h
 findtime = 1h
 maxretry = 5
````
````
$ [jeremy@web .ssh]$ sudo mv /etc/fail2ban/jail.d/00-firewalld.conf /etc/fail2ban/jail.d/00-firewalld.local
$ sudo systemctl restart fail2ban
````
````
$ sudo nano /etc/fail2ban/jail.d/sshd.local
````
[sshd]
enabled = true
bantime = 1d
maxretry = 3
````
$ sudo systemctl restart fail2ban
````

# II. Serveur Web



## 1. Reverse Proxy



🌞 **Installer NGINX**

````
$ sudo dnf install nginx
[sudo] password for jeremy:
Last metadata expiration check: 1 day, 5:14:27 ago on Tue 14 Dec 2021 10:22:44 AM CET.
Dependencies resolved.
========================================================================================================================
 Package                            Architecture  Version                                        Repository        Size
========================================================================================================================
Installing:
 nginx                              x86_64        1:1.14.1-9.module+el8.4.0+542+81547229         appstream        566 k
 [...]
   perl-threads-1:2.21-2.el8.x86_64
  perl-threads-shared-1.58-2.el8.x86_64

Complete!
$ systemctl start nginx
````


🌞 **Configurer NGINX comme reverse proxy**



🌞 **Une fois en place, text !**

- il faudra de nouveau modifier le fichier hosts de votre PC : bah oui, le nom `web.tp2.cesi` doit pointer vers l'IP du reverse proxy maintenant !
  - mais mais mais, `web.tp2.cesi` qui pointe vers l'IP d'une autre machine, le reverse proxy, c'est pas chelou ?
  - bah non :D

## 2. HTTPS

🌞 **Générer une clé et un certificat avec la commande suivante :**

```bash
$ openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 -keyout server.key -out server.crt

$ ls
server.crt  server.key
```

**Par convention dans les systèmes RedHat :**

- on met les clés privées dans `/etc/pki/tls/private/`
  - permissions très restrictives
- les certificats (clés publiques) dans `/etc/pki/tls/certs/`
  - permissions moins restrictives : généralement y'a le droit de lecture pour tout le monde

Aussi, on nomme souvent le certificat et la clé en fonction du nom de domaine pour lesquels ils sont valides, en l'occurence `web.tp2.cesi.key` pour la clé par exemple.

🌞 **Allez, faut ranger la chambre**

- déplacez au bon endroit et renommez la clé et le certificat

🌞 **Affiner la conf de NGINX**

- direction le fichier de conf de votre reverse proxy
- il faudra ajouter les lignes suivantes

```nginx
# cette ligne 'listen', vous l'avez déjà. Remplacez-la.
listen                  443 ssl http2;

# nouvelles lignes
# remplacez les chemins par la clé et le cert que vous venez de générer
ssl_certificate         /path/to/cert;
ssl_certificate_key     /path/to/key;
```

Eeet bah c'est tout.

🌞 **Test !**

- vérifiez avec votre navigateur que vous accédez en HTTPS à l'application désormais
  - alerte de sécurité : normal, le certificat est auto-signé
- `curl -k` : l'option `-k` vous permettra d'accepter les connexions TLS "non-sécurisée" : le certificat est auto-signé

🌞 **Bonus**

- **ajouter le certificat au magasin de certificat de votre navigateur** pour avoir un pitit cadenas vert
  - très utilisé en entreprise, avec des méthodes automatisées bien sûr : déployer un cert interne sur les postes client pour trust les applications internes
- **ajouter une redirection HTTP -> HTTPS**
  - c'est très courant aussi : on fait en sorte que si un client arrive en HTTP, il soit redirigé vers le site en HTTPS
  - ça permet aux clients qui arrivent en HTTP de pas manger un gros "CONNECTION REFUSED" dans leur navigateur :)
    - [hop un artiiiicle qui parle de ça](https://linuxize.com/post/redirect-http-to-https-in-nginx/) (n'hésitez pas, comme toujours, à chercher par vous-mêmes)
- **renforcer l'échange TLS en sélectionnant les algos de chiffrement à utiliser**
  - cherchez "nginx strong ciphers" :)
- **répartition de charge**
  - clonez la VM Apache + NextCloud
  - mettez en place de la répartition de charge entre les deux machines renommées `web1.tp2.cesi` et `web2 .tp2.cesi`


